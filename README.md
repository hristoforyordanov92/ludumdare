# Game projects #

These two projects were made for Ludum Dare game making competition in Unity.

## - MASCBG (Mega Awesome Super Cool Battery Game) ##

**Playable version:** https://fork0.itch.io/mega-awesome-super-cool-battery-game  
This game has been made in **12 hours**.  
**100%** of the code, art and sounds are mine.
This is my first game that ever got to a release. It does have a lot of bugs.  

## - Cat Orphanage ##

**Playable version:** https://fork0.itch.io/cat-orphanage  
This game has been made in **14-16 hours**.  
It is a collaboration between me and a friend of mine. 100% of the code is mine, where 100% of the art/sounds are his.  
This game also has tons of bugs.  