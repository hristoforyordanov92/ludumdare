﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoopScooperSounds : MonoBehaviour
{
    private AudioSource aus;
    [Header("Scoop")]
    public AudioClip[] scoopSound;
    public float scoopPitchMin = 0.8f;
    public float scoopPitchMax = 1.2f;

    private void Awake()
    {
        aus = GetComponent<AudioSource>();
    }

    public void Scoop()
    {
        aus.pitch = Random.Range(scoopPitchMin, scoopPitchMax);
        aus.PlayOneShot(scoopSound[Random.Range(0, scoopSound.Length)]);
    }
}