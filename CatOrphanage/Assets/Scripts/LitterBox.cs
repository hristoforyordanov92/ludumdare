﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LitterBox : MonoBehaviour
{
    private SpriteRenderer spr;
    public Sprite[] litterBoxSprite;
    public ELitterBoxState litterBoxState = ELitterBoxState.Empty;

    private void Awake()
    {
        spr = GetComponent<SpriteRenderer>();
        spr.sprite = litterBoxSprite[(int)litterBoxState];
    }

    public void EmptyLitterBox()
    {
        litterBoxState = ELitterBoxState.Empty;
        spr.sprite = litterBoxSprite[(int)litterBoxState];
    }

    public void FillLitterBox()
    {
        litterBoxState = ELitterBoxState.Full;
        spr.sprite = litterBoxSprite[(int)litterBoxState];
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PoopScooper")
        {
            EmptyLitterBox();
        }
    }
}