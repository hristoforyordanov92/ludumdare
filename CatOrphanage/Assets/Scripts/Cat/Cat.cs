﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour
{
    [Header("Cat Color Options")]
    public float colorMin = 0.1f;
    public float colorMax = 1f;

    [Header("Animation Options")]
    public float animationSpeedMin = 0.8f;
    public float animationSpeedMax = 1.0f;

    [Header("Cat Parts")]
    public GameObject[] catParts;
    public GameObject explosionSplatter;

    //Automatically Setup
    [HideInInspector]
    public SpriteRenderer spr;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public CatAI catAI;
    [HideInInspector]
    public CatEating catEating;
    [HideInInspector]
    public CatSounds catSounds;

    [HideInInspector]
    public Color currentColor;

    private void Awake()
    {
        spr = GetComponent<SpriteRenderer>();
        catSounds = GetComponent<CatSounds>();
        catAI = GetComponent<CatAI>();
        catEating = GetComponent<CatEating>();
        anim = GetComponent<Animator>();

        SetCatColor(RandomColor(colorMin, colorMax));
        //VinZ intrusion starts here
        anim.SetFloat("animSpeed", Random.Range(animationSpeedMin, animationSpeedMax));
        //VinZ intrusion ends here
    }

    private void Start()
    {
        Main.allCats.Add(gameObject);
        Main.currentCatCount++;
    }

    public void Explode()
    {
        catSounds.Explosion();

        Instantiate(explosionSplatter, transform.position, explosionSplatter.transform.rotation);

        for (int i = 0; i < catParts.Length; i++)
        {
            GameObject instantiatedPart = Instantiate(catParts[i], transform.position, Quaternion.identity) as GameObject;
            CatPart instantiatedPartScript = instantiatedPart.GetComponent<CatPart>();
            instantiatedPartScript.newColor = currentColor;
            instantiatedPartScript.once = true;
        }

        Main.currentCatCount--;
        Main.allCats.Remove(gameObject);
        Main.deathsCount++;
        Destroy(spr);
        Destroy(gameObject, 0.6f);
    }

    public Color RandomColor(float minValue, float maxValue)
    {
        return new Color(
            Random.Range(minValue, maxValue),
            Random.Range(minValue, maxValue),
            Random.Range(minValue, maxValue)
            );
    }

    public void SetCatColor(Color color)
    {
        currentColor = color;
        spr.color = color;
    }
}