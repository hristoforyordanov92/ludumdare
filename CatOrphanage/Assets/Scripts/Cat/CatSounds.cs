﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatSounds : MonoBehaviour
{
    private AudioSource audioSource;
    //private Cat cat;

    [Header("Meow")]
    public AudioClip[] meowSound;
    public float meowPitchMin = 0.8f;
    public float meowPitchMax = 1.4f;

    [Header("Poop")]
    public AudioClip[] poopSound;
    public float poopPitchMin = 0.8f;
    public float poopPitchMax = 1.4f;

    [Header("Explosion")]
    public AudioClip[] explosionSound;
    public float explosionPitchMin = 0.8f;
    public float explosionPitchMax = 1.2f;

    [Header("Eat")]
    public AudioClip[] eatSound;
    public float eatPitchMin = 0.8f;
    public float eatPitchMax = 1.2f;

    [Header("RandomMeow")]
    public float meowIntervalMin = 2f;
    public float meowIntervalMax = 4f;
    private float meowInterval;
    private float meowIntervalCounter;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        //cat = GetComponent<Cat>();
    }

    private void Update()
    {
        RandomMeow();
    }

    private void RandomMeow()
    {
        //if (cat.isDead == false)
        //{
        meowIntervalCounter -= Time.deltaTime;
        if (meowIntervalCounter <= 0)
        {
            Meow();
            meowInterval = Random.Range(meowIntervalMin, meowIntervalMax);
            meowIntervalCounter = meowInterval;
        }
        //}
    }

    public void Meow()
    {
        audioSource.pitch = Random.Range(meowPitchMin, meowPitchMax);
        audioSource.PlayOneShot(meowSound[Random.Range(0, meowSound.Length)]);
    }

    public void Poop()
    {
        audioSource.pitch = Random.Range(poopPitchMin, poopPitchMax);
        audioSource.PlayOneShot(poopSound[Random.Range(0, poopSound.Length)]);
    }

    public void Explosion()
    {
        audioSource.pitch = Random.Range(explosionPitchMin, explosionPitchMax);
        audioSource.PlayOneShot(explosionSound[Random.Range(0, explosionSound.Length)]);
    }

    public void Eat()
    {
        audioSource.pitch = Random.Range(eatPitchMin, eatPitchMax);
        audioSource.PlayOneShot(eatSound[Random.Range(0, eatSound.Length)], 0.2f);
    }
}