﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatAI : MonoBehaviour
{
    public Transform topLeft;
    public Transform bottomRight;

    //private CatSounds catSounds;

    private DraggableObject draggable;

    private Vector3 destination;
    private Vector3 direction;
    public GameObject currentTarget;
    public int indexToRemove;
    private bool hasDestination;
    public float movementSpeed = 1;

    public bool canEat = true;
    public float eatInterval = 1f;
    private float eatIntervalCounter;

    private float newTarget = 1.5f;
    private float newTargetCounter;

    private Rigidbody2D rb;
    private Cat cat;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        draggable = GetComponent<DraggableObject>();
        cat = GetComponent<Cat>();
        //catSounds = GetComponent<CatSounds>();

        hasDestination = false;

        eatIntervalCounter = eatInterval;
        newTargetCounter = newTarget;
        canEat = true;
    }

    private void Update()
    {
        if (draggable.isDragged == false)
        {
            if (hasDestination == false)
            {
                SetDestination();
                hasDestination = true;
            }
            else if (hasDestination == true)
            {
                //if (cat.currentHunger <= 2)
                //{
                //    hasDestination = false;
                //}
                MoveTowardDestination();
                if (Vector2.Distance(transform.position, destination) < 0.3f || ((transform.position.x > bottomRight.position.x || transform.position.x < topLeft.position.x) || (transform.position.y < bottomRight.position.y || transform.position.y > topLeft.position.y)))
                {
                    hasDestination = false;
                }
            }
        }

        if (currentTarget != null)
        {
            newTargetCounter -= Time.deltaTime;
            if (newTargetCounter <= 0)
            {
                currentTarget = null;
                newTargetCounter = newTarget;
            }
        }

        if (canEat == false)
        {
            eatIntervalCounter -= Time.deltaTime;
            if (eatIntervalCounter <= 0)
            {
                canEat = true;
                eatIntervalCounter = eatInterval;
            }
        }

        SetZPosition();
    }

    private void SetZPosition()
    {
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y,
            transform.position.y / 1000f
            );
    }

    public void MoveTowardDestination()
    {
        rb.AddForce(direction * movementSpeed, ForceMode2D.Force);
    }

    public void SetDestination()
    {
        FindDestinationCoordinates();
        FindDirection();
    }

    public void FindDestinationCoordinates()
    {
        if (cat.catEating.currentHunger <= 2 && currentTarget == null)
        {
            if (Main.allFoodBowls.Count > 0)
            {
                foreach (var foodBowl in Main.allFoodBowls)
                {
                    FoodBowl bowl = foodBowl.GetComponent<FoodBowl>();
                    if (bowl.currentFood > 1)
                    {
                        destination = bowl.transform.position;
                        currentTarget = foodBowl;
                        return;
                    }
                }
            }

            if (Main.allPoops.Count > 0)
            {
                int index = Random.Range(0, Main.allPoops.Count);
                indexToRemove = index;
                destination = Main.allPoops[index].transform.position;
                currentTarget = Main.allPoops[index];
            }
            else if (Main.allCatParts.Count > 0)
            {
                int index = Random.Range(0, Main.allCatParts.Count);
                indexToRemove = index;
                destination = Main.allCatParts[index].transform.position;
                currentTarget = Main.allCatParts[index];
            }
            else
            {
                currentTarget = null;
                destination = new Vector3(
                    Random.Range(topLeft.position.x, bottomRight.position.x),
                    Random.Range(bottomRight.position.y, topLeft.position.y),
                    0
                    );
            }
        }
        else
        {
            currentTarget = null;
            destination = new Vector3(
                Random.Range(topLeft.position.x, bottomRight.position.x),
                Random.Range(bottomRight.position.y, topLeft.position.y),
                0
                );
        }

    }

    public void FindDirection()
    {
        direction = destination - transform.position;
    }

}