﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatPooping : MonoBehaviour
{
    [Header("Poop")]
    public GameObject poopPrefab;
    public int poopPoints = 0;
    public int poopPointsMax = 2;

    [HideInInspector]
    private Cat cat;
    [HideInInspector]
    private CatSounds catSounds;

    private void Awake()
    {
        cat = GetComponent<Cat>();
        catSounds = cat.catSounds;
    }

    public void Poop()
    {
        Instantiate(poopPrefab, transform.position, Quaternion.identity);
        catSounds.Poop();
    }

    public void GainPoopPoints(int pointsAmount)
    {
        poopPoints++;
        if (poopPoints >= poopPointsMax)
        {
            Poop();
            poopPoints = 0;
        }
    }
}