﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatEating : MonoBehaviour
{
    [Header("Eating")]
    public int maxHunger = 5;
    public int currentHunger;
    public int loseHungerValue = 1;
    public float loseHungerAfter = 5f;
    public bool useMinMaxTimes = false;
    public float loseHungerAfterMin = 4f;
    public float loseHungerAfterMax = 6f;
    private float loseHungerAfterCounter;
    private bool canEat = true;
    public bool CanEat { get { return canEat; } }
    public float canEatTimer = 0.5f;
    private float canEatTimerCounter;
    public bool canEatPoop;
    public bool canEatCatParts;

    [Header("Toxicity")]
    public int maxToxicity = 5;
    public int currentToxicity;
    public int toxicityPoop = 1;
    public int toxicityCatPart = 1;

    [Header("Poop")]
    public GameObject poopPrefab;
    public int poopPoints = 0;
    public int poopPointsMax = 2;

    [Header("Explosion")]
    public GameObject[] catParts;
    public GameObject explosionSplatter;

    private bool isAlive = true;

    private Cat cat;
    private CatSounds catSounds;

    private void Awake()
    {
        currentHunger = maxHunger;
        if (useMinMaxTimes == true)
        {
            loseHungerAfter = Random.Range(loseHungerAfterMin, loseHungerAfterMax);
        }
        loseHungerAfterCounter = loseHungerAfter;
        canEat = true;
        canEatTimerCounter = canEatTimer;

        currentToxicity = 0;

        poopPoints = 0;

        isAlive = true;

        canEatPoop = false;
        canEatCatParts = false;

        cat = GetComponent<Cat>();
        catSounds = cat.catSounds;
    }

    private void Update()
    {
        LoseHungerInTime();
        CanEatAfterTime();
    }

    public void LoseHunger(int amount)
    {
        if (isAlive)
        {
            currentHunger -= amount;
            if (currentHunger < 2)
            {
                canEatPoop = true;
                canEatCatParts = true;
            }

            GainPoopPoint(1);
            if (currentHunger <= 0)
            {
                Explode();
            }
        }
    }

    public void GainPoopPoint(int amount)
    {
        poopPoints += amount;
        if (poopPoints >= poopPointsMax)
        {
            Poop();
            poopPoints = 0;
        }
    }

    public void LoseHungerInTime()
    {
        loseHungerAfterCounter -= Time.deltaTime;
        if (loseHungerAfterCounter <= 0)
        {
            LoseHunger(loseHungerValue);
            if (useMinMaxTimes == true)
            {
                loseHungerAfter = Random.Range(loseHungerAfterMin, loseHungerAfterMax);
            }
            loseHungerAfterCounter = loseHungerAfter;
        }
    }

    public void GainToxicity(string tag)
    {
        if (tag == "Poop")
        {
            currentToxicity += toxicityPoop;
        }
        else if (tag == "CatPart")
        {
            currentToxicity += toxicityCatPart;
        }

        if (currentToxicity >= maxToxicity)
        {
            Explode();
        }
    }

    public void Explode()
    {
        catSounds.Explosion();
        isAlive = false;

        Instantiate(explosionSplatter, transform.position, explosionSplatter.transform.rotation);

        foreach (var catPart in catParts)
        {
            GameObject instantiatedPart = Instantiate(catPart, transform.position, Quaternion.identity);
            CatPart instantiatedPartScript = instantiatedPart.GetComponent<CatPart>();
            instantiatedPartScript.newColor = cat.currentColor;
            instantiatedPartScript.once = true;
        }

        Main.currentCatCount--;
        Main.allCats.Remove(gameObject);
        Main.deathsCount++;

        Destroy(cat.spr);
        Destroy(gameObject, 0.5f);
    }

    public void Poop()
    {
        Instantiate(poopPrefab, transform.position, Quaternion.identity);
        catSounds.Poop();
    }

    public void CanEatAfterTime()
    {
        if (canEat == false)
        {
            canEatTimerCounter -= Time.deltaTime;
            if (canEatTimerCounter <= 0)
            {
                canEatTimerCounter = canEatTimer;
                canEat = true;
            }
        }
    }

    public void GainHunger(int amount)
    {
        canEat = false;
        currentHunger += amount;
        catSounds.Eat();
        if (currentHunger > 2)
        {
            canEatPoop = false;
            canEatCatParts = false;
        }
        if (currentHunger >= maxHunger)
        {
            currentHunger = maxHunger;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "FoodBowl" && isAlive && canEat)
        {
            FoodBowl foodBowl = collision.gameObject.GetComponent<FoodBowl>();
            if (currentHunger < maxHunger && foodBowl.foodBowlState != EFoodBowlState.Empty)
            {
                GainHunger(1);
                foodBowl.EmptyCupOnce();
            }
        }
        else if (collision.gameObject.tag == "Poop" && canEatPoop)
        {
            Poop poop = collision.gameObject.GetComponent<Poop>();
            poop.EatPoop();
            GainHunger(1);
            GainToxicity(collision.gameObject.tag);
        }
        else if (collision.gameObject.tag == "CatPart" && canEatCatParts)
        {
            CatPart catPart = collision.gameObject.GetComponent<CatPart>();
            catPart.EatCatPart();
            GainHunger(1);
            GainToxicity(collision.gameObject.tag);
        }
    }
}