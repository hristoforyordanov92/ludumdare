﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poop : MonoBehaviour
{
    private SpriteRenderer spr;
    public Sprite[] poopSprite;

    private void Awake()
    {
        spr = GetComponent<SpriteRenderer>();
        spr.sprite = poopSprite[Random.Range(0, poopSprite.Length)];
    }

    private void Start()
    {
        Main.allPoops.Add(gameObject);
    }

    public void EatPoop()
    {
        Main.allPoops.Remove(gameObject);
        Destroy(gameObject);
    }
}