﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodSplatter : MonoBehaviour
{
    private SpriteRenderer spr;

    public float fadeInSeconds = 10f;

    private void Awake()
    {
        spr = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        fadeInSeconds -= Time.deltaTime;
        Color oldColor = spr.color;
        spr.color = new Color(oldColor.r, oldColor.g, oldColor.b, 0.1f * fadeInSeconds);
        if (fadeInSeconds <= 0)
        {
            Destroy(gameObject);
        }
    }
}