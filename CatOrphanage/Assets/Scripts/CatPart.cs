﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatPart : MonoBehaviour
{
    private SpriteRenderer spr;
    private Rigidbody2D rb;

    public Transform bottomRight;
    public Transform topLeft;

    public Color newColor;

    public Vector3 direction;

    public bool once;

    private void Awake()
    {
        spr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        rb.angularDrag = 1f;
        rb.drag = 0.5f;
        once = true;
    }

    private void Start()
    {
        Main.allCatParts.Add(gameObject);
    }

    private void Update()
    {
        if (once)
        {
            spr.color = newColor;
            direction = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            rb.AddForce(direction * 30f, ForceMode2D.Force);
            rb.angularVelocity = direction.x * -100f;
            once = false;
        }

        SetZPosition();

        if ((transform.position.x > bottomRight.position.x || transform.position.x < topLeft.position.x) || (transform.position.y < bottomRight.position.y || transform.position.y > topLeft.position.y))
        {
            rb.velocity = Vector3.zero;
        }
    }

    public void EatCatPart()
    {
        Main.allCatParts.Remove(gameObject);
        Destroy(gameObject);
    }

    private void SetZPosition()
    {
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y,
            transform.position.y / 1000f
            );
    }
}