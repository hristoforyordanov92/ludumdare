﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableObject : MonoBehaviour
{
    private Vector3 transformPos;
    public bool isDragged;
    private bool calculateOnce;

    private void Awake()
    {
        isDragged = false;
        calculateOnce = true;
    }

    private void Update()
    {
        if (isDragged)
        {
            if (Input.GetMouseButtonUp(0))
            {
                isDragged = false;
                calculateOnce = true;
            }
            DragItem();
        }
    }

    private void OnMouseDown()
    {
        isDragged = true;
    }

    private void OnMouseUp()
    {
        isDragged = false;
        calculateOnce = true;
    }

    public void DragItem()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
        if (calculateOnce)
        {
            transformPos = transform.position;
            transformPos = transformPos - mousePos;
            calculateOnce = false;
        }

        Vector3 newPos = mousePos + transformPos;
        transform.position = new Vector3(newPos.x, newPos.y, transform.position.y / 1000f);
    }
}