﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public GameObject endScreen;
    public Text text;

    public static int deathsCount = 0;

    public static List<GameObject> allPoops;
    public static List<GameObject> allCats;
    public static List<GameObject> allFoodBowls;
    public static List<GameObject> allFoods;
    public static List<GameObject> allCatParts;

    public Transform catSpawnPoint;
    public GameObject catPrefab;

    public int maxCatCount = 10;
    public static int currentCatCount = 0;
    public int curCatCount;
    public float spawnCatTimer = 5;
    private float spawnCatTimerCounter;

    private void Awake()
    {
        allPoops = new List<GameObject>();
        allCats = new List<GameObject>();
        allFoodBowls = new List<GameObject>();
        allFoods = new List<GameObject>();
        allCatParts = new List<GameObject>();

        currentCatCount = 0;
        spawnCatTimerCounter = spawnCatTimer;
    }

    private void Update()
    {
        SpawnCatTimer();

        text.text = "DEATHS: " + deathsCount.ToString();

        if (deathsCount >= 20)
        {
            maxCatCount = 0;
            endScreen.SetActive(true);
        }

        curCatCount = currentCatCount;
    }

    public void NewGame()
    {
        deathsCount = 0;
        endScreen.SetActive(false);
        SceneManager.LoadScene("Game");
    }

    public void MainMenu()
    {
        deathsCount = 0;
        endScreen.SetActive(false);
        SceneManager.LoadScene("Title");
    }

    public void SpawnCat()
    {
        Instantiate(catPrefab, catSpawnPoint.position, Quaternion.identity);
    }

    public void SpawnCatTimer()
    {
        spawnCatTimerCounter -= Time.deltaTime;
        if (spawnCatTimerCounter <= 0)
        {
            if (currentCatCount < maxCatCount)
            {
                SpawnCat();
                spawnCatTimerCounter = spawnCatTimer;
            }
        }
    }
}