﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBowl : MonoBehaviour
{
    //When choosing which sprite to put first, go for the same order as the enum EFoodBowlState.
    //Empty should correspond to index 0 of the Sprite array.
    public Sprite[] foodBowlSprite;
    private SpriteRenderer spr;

    public EFoodBowlState foodBowlState = EFoodBowlState.Empty;

    public int maxFood = 10;
    public int currentFood;
    public int filledFoodPerFoodItem = 2;
    public int foodValue = 1;

    private void Awake()
    {
        //
        spr = GetComponent<SpriteRenderer>();
        currentFood = maxFood;
        RefreshSprite();
        //cupState = ECupState.Empty;
    }

    private void Start()
    {
        Main.allFoodBowls.Add(gameObject);
    }

    private void Update()
    {
        //spriteR.sprite = foodSprite[(int)foodState];
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "ChickenLeg")
        {
            if (currentFood < maxFood)
            {
                FillCupOnce();
                Main.allFoods.Remove(other.gameObject);
                Destroy(other.gameObject);
            }
        }
    }

    public void FillCupMax()
    {
        foodBowlState = EFoodBowlState.Full;
        RefreshSprite();
    }

    public void FillCupOnce()
    {
        currentFood += filledFoodPerFoodItem;
        if (currentFood > maxFood)
        {
            currentFood = maxFood;
        }
        RefreshSprite();
    }

    public void EmptyCupMax()
    {
        foodBowlState = EFoodBowlState.Empty;
        RefreshSprite();
    }

    public void EmptyCupOnce()
    {
        currentFood -= foodValue;
        if (currentFood <= 0)
        {
            currentFood = 0;
        }
        RefreshSprite();
    }

    public void RefreshSprite()
    {
        if (currentFood > maxFood / 2)
        {
            foodBowlState = EFoodBowlState.Full;
        }
        else if (currentFood > 1)
        {
            foodBowlState = EFoodBowlState.HalfFull;
        }
        else
        {
            foodBowlState = EFoodBowlState.Empty;
        }
        spr.sprite = foodBowlSprite[(int)foodBowlState];
    }
}