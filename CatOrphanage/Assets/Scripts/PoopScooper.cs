﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoopScooper : MonoBehaviour
{
    private PoopScooperSounds poopScooperSounds;

    private void Awake()
    {
        poopScooperSounds = GetComponentInParent<PoopScooperSounds>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Poop")
        {
            poopScooperSounds.Scoop();
            Main.allPoops.Remove(collision.gameObject);
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "CatPart")
        {
            poopScooperSounds.Scoop();
            Main.allCatParts.Remove(collision.gameObject);
            Destroy(collision.gameObject);
        }
    }
}