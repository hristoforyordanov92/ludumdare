﻿public enum EFoodBowlState
{
    Empty = 0,
    HalfFull,
    Full
}