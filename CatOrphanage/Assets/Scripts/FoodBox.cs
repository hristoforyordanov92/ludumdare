﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBox : MonoBehaviour
{
    public GameObject chickenLeg;

    private bool canDestroyFood = true;
    private float canDestroyFoodTime = 0.1f;
    private float canDestroyFoodTimeCounter;

    private void Awake()
    {
       canDestroyFoodTimeCounter = canDestroyFoodTime;
    }

    private void Update()
    {
        if (canDestroyFood == false)
        {
            canDestroyFoodTimeCounter -= Time.deltaTime;
            if (canDestroyFoodTimeCounter <= 0)
            {
                canDestroyFood = true;
                canDestroyFoodTimeCounter = canDestroyFoodTime;
            }
        }
    }

    private void OnMouseDown()
    {
        GameObject instantiatedChickenLeg = SpawnChickenLeg();
        instantiatedChickenLeg.GetComponent<DraggableObject>().isDragged = true;
    }

    public GameObject SpawnChickenLeg()
    {
        canDestroyFood = false;
        Vector3 spawnPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return Instantiate(
            chickenLeg,
            new Vector3(spawnPosition.x, spawnPosition.y, 10),
            Quaternion.identity
            );
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "ChickenLeg" && canDestroyFood)
        {
            Main.allFoods.Remove(other.gameObject);
            Destroy(other.gameObject);
        }
    }
}