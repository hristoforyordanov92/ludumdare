﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public AudioClip die;
    public AudioClip button;

    private AudioSource aus;

    //veri informative comment
    public static int step;
    public static bool endGame;
    public Transform player;
    public GameObject platform;
    public Transform levelParent;

    public GameObject smallBattery;
    public GameObject mediumBattery;
    public GameObject bigBattery;
    public GameObject hugeBattery;

    public GameObject gameOverPanel;

    public GameObject wall;

    public int platformLength;
    public int batteryOffset;

    private bool once;

    //this is the awake method.
    void Awake()
    {
        aus = GetComponent<AudioSource>();
        step = -4;
        platformLength = 19;
        batteryOffset = 9;
        endGame = false;
        once = true;
    }

    //this is the start method. it's super important
    void Start()
    {

    }

    //this method is not like the other ones
    void Update()
    {
        if (player.transform.position.x > step * (6))
        {
            SpawnPlatforms();
        }

        EndGame();
    }

    //this comment is pointless
    void EndGame()
    {
        if (endGame)
        {
            if (once)
            {
                aus.PlayOneShot(die);
                gameOverPanel.SetActive(true);
                player.gameObject.SetActive(false);


                once = false;
            }
        }
    }

    void SpawnPlatforms()
    {
        int firstPlatformY = Random.Range(12, 1);
        int secondPlatformY = Random.Range(-1, -12);
        int batteryY = 0;

        switch (Random.Range(1, 4))
        {
            case 1:
                batteryY = Random.Range(13, firstPlatformY);
                break;
            case 2:
                batteryY = Random.Range(firstPlatformY - 1, secondPlatformY);
                break;
            case 3:
                batteryY = Random.Range(secondPlatformY, -14);
                break;
            default:
                Debug.Log("BatteryY finder: default case;\n if this prints out, i will be surprised");
                break;
        }//this is a closing curly bracket. idk if i spelled it correctly, because i can't really spell good american language

        switch (Random.Range(1, 3))
        {
            case 1:
                SpawnUpperPlatform(firstPlatformY, secondPlatformY);
                break;
            case 2:
                SpawnLowerPlatform(firstPlatformY, secondPlatformY);
                break;
            default:
                break;
        }

        if (Random.Range(1, 100) < 35)
        {
            Instantiate(wall, new Vector3((step + 12) * platformLength, Random.Range(-12, 12)), Quaternion.identity, levelParent);
        }

        if (Random.Range(1, 100) < 45)
        {
            switch (Random.Range(1, 5))
            {
                case 1:
                    Instantiate(smallBattery, new Vector3(((step + 12) * platformLength) - batteryOffset, batteryY, 0), Quaternion.identity, levelParent);
                    break;
                case 2:
                    Instantiate(mediumBattery, new Vector3(((step + 12) * platformLength) - batteryOffset, batteryY, 0), Quaternion.identity, levelParent);
                    break;
                case 3:
                    Instantiate(bigBattery, new Vector3(((step + 12) * platformLength) - batteryOffset, batteryY, 0), Quaternion.identity, levelParent);
                    break;
                case 4:
                    Instantiate(hugeBattery, new Vector3(((step + 12) * platformLength) - batteryOffset, batteryY, 0), Quaternion.identity, levelParent);
                    break;
                default:
                    Debug.Log("Battery Spawner: default case;\n if this prints out, i will be surprised");
                    break;
            }
        }
        step += 1;
    }

    void SpawnUpperPlatform(int _firstPlatY, int _secondPlatY)
    {
        Instantiate(platform, new Vector3((step + 12) * platformLength, _firstPlatY, 0), Quaternion.identity, levelParent);
        if (Random.Range(1, 100) < 55)
        {
            Instantiate(platform, new Vector3((step + 12) * platformLength, _secondPlatY, 0), Quaternion.identity, levelParent);
        }
    }

    void SpawnLowerPlatform(int _firstPlatY, int _secondPlatY)
    {
        Instantiate(platform, new Vector3((step + 12) * platformLength, _secondPlatY, 0), Quaternion.identity, levelParent);
        if (Random.Range(1, 100) < 55)
        {
            Instantiate(platform, new Vector3((step + 12) * platformLength, _firstPlatY, 0), Quaternion.identity, levelParent);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void ToTitle()
    {
        SceneManager.LoadScene("Title");
    }

    public void PlayButton()
    {
        aus.PlayOneShot(button);
    }
}