﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBattery : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public Image filler;
    public Text text;

    void Update()
    {
        //i know this calculation is done with floats and the result is a float. also the small performance gain from using a float instead of var(i think), but i am using var,
        //because it's so much easier to write var than float. shorter and faster. i definetely saved tons of time doing it!
        var v = playerMovement.batteryPower / playerMovement.batteryPowerMax;
        filler.fillAmount = v;
        filler.color = new Color(1 - v, v, 0);
        text.color = new Color(1 - v, v, 0);
    }
}