﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public AudioClip gainPower;
    public AudioClip losePower;
    public AudioClip die;
    public AudioClip overcharge;

    private AudioSource aus;

    private Rigidbody2D rb;
    private PlayerJump pj;

    public float batteryPower;
    public float batteryPowerMax;
    public float overchargePower;

    public float moveSpeedMin;
    public float moveSpeedMax;
    public float moveSpeed;

    public bool canDrain;
    public bool isOvercharged;

    public float cooldown;
    public float cooldownCounter;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        pj = GetComponent<PlayerJump>();
        aus = GetComponent<AudioSource>();
        batteryPower = 10f;
        canDrain = false;
        batteryPowerMax = 50;
        isOvercharged = false;

        cooldown = 5;
        cooldownCounter = 0;

        moveSpeedMin = 4;
        moveSpeedMax = 25;
    }

    public void PowerDrain()
    {
        if (canDrain)
        {
            //batteryPower *= 0.999f;
            if ((batteryPower / 16) * Time.deltaTime > 0.5f * Time.deltaTime)
            {
                batteryPower -= (batteryPower / 16) * Time.deltaTime;
            }
            else
            {
                batteryPower -= 0.5f * Time.deltaTime;
            }

            if (batteryPower <= 0)
            {
                LevelController.endGame = true;
                batteryPower = 0;
                canDrain = false;
            }
        }
    }

    public void LoseOvercharge()
    {
        if (isOvercharged)
        {
            cooldownCounter -= Time.deltaTime;
            if (cooldownCounter <= 0)
            {
                isOvercharged = false;
                canDrain = true;
            }
        }
    }

    public void LosePower(float _power)
    {
        isOvercharged = false;
        cooldownCounter = 0;
        canDrain = true;
        batteryPower -= _power;
        if (batteryPower <= 0)
        {
            batteryPower = 0;
        }
        aus.PlayOneShot(losePower);
    }

    public void GainPower(float _power)
    {
        pj.GainJump();
        if (batteryPower == batteryPowerMax)
        {
            overchargePower += _power * 10;
            cooldownCounter = cooldown;
            isOvercharged = true;
            canDrain = false;
            aus.PlayOneShot(overcharge);

        }
        else if (batteryPower + _power >= batteryPowerMax)
        {
            if (batteryPower < batteryPowerMax)
            {
                batteryPower = batteryPowerMax;
                overchargePower += (_power - (batteryPowerMax - batteryPower)) * 10;
                cooldownCounter = cooldown;
                isOvercharged = true;
                canDrain = false;
                aus.PlayOneShot(overcharge);

            }
            else
            {
                batteryPower = batteryPowerMax;
                cooldownCounter = cooldown;
                isOvercharged = true;
                canDrain = false;
                aus.PlayOneShot(overcharge);


            }
        }
        else
        {
            batteryPower += _power;
            aus.PlayOneShot(gainPower);

        }
    }

    void Update()
    {
        if (LevelController.step == 22)
        {
            canDrain = true;
        }
        PowerDrain();
        LoseOvercharge();
        Die();
    }

    void FixedUpdate()
    {
        Movement(moveSpeed, moveSpeedMin, moveSpeedMax);
    }

    void Movement(float _moveSpeed, float _moveSpeedMin, float _moveSpeedMax)
    {
        var vel = rb.velocity;
        _moveSpeed = batteryPower;
        var ms = Mathf.Clamp(_moveSpeed, _moveSpeedMin, _moveSpeedMax);



        vel.x = ms;
        rb.velocity = vel;
    }

    void Die()
    {
        if (transform.position.y < -30 || transform.position.y > 30)
        {
            //aus.PlayOneShot(die);
            LevelController.endGame = true;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "SmallBattery")
        {
            GainPower(2);
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "MediumBattery")
        {
            GainPower(4);
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "BigBattery")
        {
            GainPower(7);
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag == "HugeBattery")
        {
            GainPower(12);
            Destroy(collision.gameObject);
        }
    }
}