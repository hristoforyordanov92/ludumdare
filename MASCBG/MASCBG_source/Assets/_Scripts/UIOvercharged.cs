﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOvercharged : MonoBehaviour
{
    public PlayerMovement plmv;
    public Image objectToEnable;
    public Image filler;
    public Text text;
    public Image mask;

    private void Awake()
    {
        text.text = "O\nV\nE\nR\nC\nH\nA\nR\nG\nE\nD";
    }

    void Update()
    {
        var v = plmv.cooldownCounter / plmv.cooldown;
        if (plmv.isOvercharged)
        {

            objectToEnable.enabled = true;
            mask.enabled = true;
            filler.enabled = true;
            text.enabled = true;

            filler.fillAmount = v;
            filler.color = new Color(1 - v, v, 0);
            text.color = new Color(1 - v, v, 0);


        }
        else
        {
            objectToEnable.enabled = false;
            mask.enabled = false;
            filler.enabled = false;
            text.enabled = false;
        }


    }

    //void ColorCalculation(ref bool _bool, ref float _color)
    //{
    //    if (!_bool)
    //    {
    //        _color += Time.deltaTime;
    //        if (_color >= 1)
    //        {
    //            _color = 1;
    //            _bool = true;
    //        }
    //    }
    //    else if (_bool)
    //    {
    //        _color -= Time.deltaTime;
    //        if (_color <= 0)
    //        {
    //            _color = 0;
    //            _bool = false;
    //        }
    //    }
    //}
}