﻿using UnityEngine;

public class DestroyOnInvisible : MonoBehaviour
{
    private float secondsToDestroy;
    private bool canBeDestoyed;

    private BoxCollider2D bc;
    private Rigidbody2D rb;

    void Awake()
    {
        bc = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        canBeDestoyed = false;
        secondsToDestroy = 5;
    }

    public void DestroyFromTrigger()
    {
        bc.enabled = false;
        rb.bodyType = RigidbodyType2D.Dynamic;
        rb.gravityScale = 1;
    }

    private void OnBecameVisible()
    {
        canBeDestoyed = true;
    }

    private void OnBecameInvisible()
    {
        if (canBeDestoyed)
        {
            Destroy(gameObject, secondsToDestroy);
        }
    }
}