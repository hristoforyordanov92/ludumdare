﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    public AudioClip jump;

    private AudioSource aus;

    private Rigidbody2D rb;
    private PlayerMovement plmv;

    public int jumps;
    public int maxJumps;
    private bool canJump;
    //private float gravity;

    void Awake()
    {
        plmv = GetComponent<PlayerMovement>();
        rb = GetComponent<Rigidbody2D>();
        aus = GetComponent<AudioSource>();

        canJump = true;
        //gravity = -10;
        maxJumps = 3;
        jumps = maxJumps;
    }

    void Update()
    {
        MovementCheck();
    }

    void MovementCheck()
    {
        if (Input.GetKeyDown(KeyCode.Space) && canJump)
        {
            Physics2D.gravity *= -1;
            //gravity *= -1;
            var vel = rb.velocity;
            vel.y = 0;
            rb.velocity = vel;
            rb.AddForce(new Vector2(0, Physics2D.gravity.y), ForceMode2D.Impulse);
            jumps -= 1;
            aus.PlayOneShot(jump);
            if (jumps <= 0)
            {
                canJump = false;
            }
        }
    }

    public void GainJump()
    {
        if (jumps < maxJumps)
        {
            jumps += 1;

        }
        else
        {
            jumps = maxJumps;
        }
    }

    private void FixedUpdate()
    {
        //var vel = rb.velocity;
        //vel.y = gravity;
        //rb.velocity = vel;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            jumps = maxJumps;
            canJump = true;
        }

        if (collision.gameObject.tag == "Wall")
        {
            collision.gameObject.GetComponent<DestroyOnInvisible>().DestroyFromTrigger();
            plmv.LosePower(5);
        }
    }
}