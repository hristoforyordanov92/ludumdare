﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenScore : MonoBehaviour
{
    private Text score;
    public PlayerMovement plmv;

    private void OnEnable()
    {
        score.text = "\n\n\nSCORE\n" + plmv.overchargePower.ToString();
    }
    void Awake()
    {
        score = GetComponent<Text>();
    }

    void Update()
    {

    }
}