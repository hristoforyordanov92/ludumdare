﻿using UnityEngine;

[ExecuteInEditMode]
public class CameraMovement : MonoBehaviour
{
    public GameObject target;
    private Camera cam;
    private PlayerMovement playerMove;
    // private Rigidbody2D rb;
    private Vector3 targetVector;
    public float offset;

    public float moveSpeed = 7;

    public float camZoomMin;
    public float camZoomMax;

    private void Awake()
    {
        offset = 5.5f;
        cam = gameObject.GetComponent<Camera>();
        playerMove = target.GetComponent<PlayerMovement>();
        //rb = target.GetComponent<Rigidbody2D>();

        camZoomMin = 15;
        camZoomMax = 20;
    }

    void Update()
    {

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
        else
        {
            var v = Mathf.Lerp(cam.orthographicSize, playerMove.batteryPower, 0.5f * Time.deltaTime);
            cam.orthographicSize = Mathf.Clamp(v, camZoomMin, camZoomMax);

            targetVector = new Vector3(target.transform.position.x + cam.orthographicSize + 5, transform.position.y, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, targetVector, moveSpeed / 4 * Time.deltaTime);
        }
    }
}