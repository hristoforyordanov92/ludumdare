﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitingTrigger : MonoBehaviour
{
    void Start()
    {
		
    }

    void Update()
    {
		
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            collision.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }
}