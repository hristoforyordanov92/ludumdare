﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public AudioClip button;
    private AudioSource aus;

    private void Awake()
    {
        aus = GetComponent<AudioSource>();
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void PlaySound()
    {
        aus.PlayOneShot(button);
    }
}